﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointsController : MonoBehaviour
{
    // Start is called before the first frame update
    private int points;
    private GameObject pointsObject;
    private TextMesh pointsMesh;
    void Start()
    {
        points = 0;
        pointsMesh = GetComponent<TextMesh>();
        Debug.Log("text: " + pointsMesh.text);
        UpdatePoints();
    }

    private void UpdatePoints()
    {
        pointsMesh.text = points.ToString("D4");
    }

    public void AddPoint()
    {
        points += 1;
        UpdatePoints();
    }
}
