﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;

public class Activator : MonoBehaviour
{
    public KeyCode key;
    private PointsController pointsController;
    private bool active = false;
    private GameObject note;
    void Start()
    {
        pointsController = GameObject.Find("Points").GetComponent<PointsController>();
    }
    
    void Update()
    {
        if (Input.GetKeyDown(key) && active)
        {
            Destroy(note);
            pointsController.AddPoint();
            print("Destroying");
            active = false;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        active = true;
        if (other.gameObject.CompareTag("Note"))
        {
            note = other.gameObject;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        active = false;
    }
}
