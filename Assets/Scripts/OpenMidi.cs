﻿using System;
using System.Collections;
using System.Threading;
using System.Collections.Generic;
using Melanchall.DryWetMidi.Core;
using mcore = Melanchall.DryWetMidi.Core;
using Melanchall.DryWetMidi.Devices;
using Melanchall.DryWetMidi.Interaction;
using UnityEngine;

public class OpenMidi : MonoBehaviour
{
    private sealed class ThreadTickGenerator : ITickGenerator
    {
        public event EventHandler TickGenerated;

        private Thread _thread;

        public void TryStart()
        {
            if (_thread != null)
                return;

            _thread = new Thread(() =>
            {
                var stopwatch = new System.Diagnostics.Stopwatch();
                var lastMs = 0L;

                stopwatch.Start();

                while (true)
                {
                    var elapsedMs = stopwatch.ElapsedMilliseconds;
                    if (elapsedMs - lastMs >= 1)
                    {
                        TickGenerated?.Invoke(this, EventArgs.Empty);
                        lastMs = elapsedMs;
                    }
                }
            });

            _thread.Start();
        }

        public void Dispose()
        {
        }
    }

    public GameObject spawn1, activator1;
    private bool isPlaying = false;
    public int channel = 1;
    public float speed = 6f;
    private Playback _playback;
    //private NoteFactory spawnNoteFactory;
    private Dictionary<String, NoteFactory> SpawnDict = new Dictionary<String, NoteFactory>();
    private GameObject[] gameObjects;
    private String noteName;
    private NoteFactory noteFactoryTmp; 
    private TempoMap tempoMap;
    private MetricTimeSpan metricTime;
    private PlayMidi playMidi;
    private float timeToStart;

    // Start is called before the first frame update
    void Start()
    {
        gameObjects = GameObject.FindGameObjectsWithTag("Spawn");
        playMidi = gameObject.GetComponent<PlayMidi>();
        timeToStart = getTimeToStart();

        //Debug.Log("size: " + gameObjects.Length);

        foreach(GameObject spawn in gameObjects)
        {
            //Debug.Log("Parent: " + spawn.transform.parent.name);
            foreach(String note in spawn.GetComponent<ChoseNotesToPlay>().GetNotesList())
            {
                if( spawn.GetComponent<NoteFactory>())
                {
                    SpawnDict.Add(note, spawn.GetComponent<NoteFactory>());
                    //Debug.Log("note: " + note);
                }
            }
        }
        var midiFile = MidiFile.Read("Assets/MidiFiles/ozzy_osbourne.mid");
        var notes = midiFile.GetNotes();
        tempoMap = midiFile.GetTempoMap();

        foreach(var channelTmp in mcore.MidiFileUtilities.GetChannels(midiFile))
            Debug.Log("Channel: " + channelTmp);

        _playback = midiFile.GetPlayback(new MidiClockSettings
        {
            CreateTickGeneratorCallback = _ => new ThreadTickGenerator()
        });

        _playback.NotesPlaybackFinished += Test;
        _playback.InterruptNotesOnStop = true;
        StartCoroutine(StartMusic());
        
        playMidi.SetTimeToPlay(timeToStart);
    }

    private void Test(object sender, NotesEventArgs notesArgs)
    {
        var notesList = notesArgs.Notes;
        foreach (Note note in notesList)
        {
            if (!isPlaying)
            {
                //playMidi.SetTimeToPlay(timeToStart);
                isPlaying = true;
            }

            if (note.Channel == channel) 
            {
                noteName = Char.ToString(note.NoteName.ToString()[0]);
                metricTime = note.LengthAs<MetricTimeSpan>(tempoMap);
                Debug.Log("chann: " + note.Channel + " note: " + noteName + " len:" + metricTime);

                if( SpawnDict.TryGetValue(noteName, out noteFactoryTmp))
                {
                    noteFactoryTmp.SetNote((TimeSpan)metricTime);
                }
            }
        }
    }

    private float getTimeToStart()
    {
        float spawnDistance = Math.Abs(Math.Abs(spawn1.transform.position.x) - Math.Abs(activator1.transform.position.x));
        Debug.Log("spawn1: " + spawn1.transform.position.x);
        Debug.Log("activator1: " + activator1.transform.position.x);
        Debug.Log("speed: " + this.speed);
        Debug.Log("spawn dist: " + spawnDistance + " inTime: " + spawnDistance/this.speed);
        return (spawnDistance/this.speed);
    }
    private IEnumerator StartMusic()
    {
        _playback.Start();
        while (_playback.IsRunning)
        {
            yield return null;

        }
        _playback.Dispose();
    }

    private void OnApplicationQuit()
    {
        _playback.Stop();
        _playback.Dispose();
    }
}