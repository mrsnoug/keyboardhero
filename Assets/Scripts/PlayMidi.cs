﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MidiPlayerTK;
public class PlayMidi : MonoBehaviour
{
    // Start is called before the first frame update
    public int index = 65;
    private MidiFilePlayer midiFilePlayer;
    private bool playMidi = false, playing = false;
    private float timeToPlay;

    void Awake()
    {
        midiFilePlayer = FindObjectOfType<MidiFilePlayer>();
        midiFilePlayer.MPTK_MidiIndex = index;
    }
    // Update is called once per frame
    void Update()
    {
        if(playMidi && !playing)
        {
            playing = true;
            Debug.Log("PlayMidiInTime: " + this.timeToPlay);
            Invoke("PlayMidiInTime", this.timeToPlay);
        }
    }

    private void PlayMidiInTime()
    {
        midiFilePlayer.MPTK_Play();
    }

    public void SetTimeToPlay(float time)
    {
        playMidi = true;
        this.timeToPlay = time;

    }
}
