﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrefabTest : MonoBehaviour
{
    public GameObject prefab;

    public KeyCode spawnKeyCode;
    public GameObject spawnPoint;

    private Vector3 coords;
    // Start is called before the first frame update
    void Start()
    {
        Vector3 spawn = spawnPoint.transform.position;
        spawn.y += 0.5f;
        coords = spawn;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(spawnKeyCode))
        {
            Instantiate(prefab, coords, Quaternion.identity);
        }
        
    }
}
