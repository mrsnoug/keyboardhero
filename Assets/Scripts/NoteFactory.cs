﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEditor;

public class NoteFactory : MonoBehaviour
{
    public KeyCode spawn_key;
    public GameObject notePrefab;
    public GameObject spawn_point;
    private float speed = 6f;
    private bool createNewNote = false;
    private float time, noteLength;
    //private GameObject cubePref;

    //private PrimitiveType notePrefab = PrimitiveType.Cube; 
    private Vector3 coords;
    void Start()
    {
        time = Time.time;
        Vector3 spawn = spawn_point.transform.position;
        spawn.y += 0.5f;
        coords = spawn;
        //cubePref = GameObject.Find("TestNote");
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(spawn_key))
        {
            NewNote();
        }
        
        // if (createNewNote)
        // {
        //     if(Time.time - time > 1.1)
        //     {
        //         time = Time.time;
        //         NewNote();
        //     }
        // }
        if (createNewNote)
            NewNote();
    }

    private void NewNote()
    {
        createNewNote = false;
	    // GameObject newNote = GameObject.CreatePrimitive(PrimitiveType.Cube);
     //    newNote.transform.position = coords;
     //    newNote.AddComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotationX | 
     //    RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ |
     //    RigidbodyConstraints.FreezePositionZ;
     //
     //    newNote.transform.localScale = new Vector3(noteLength, newNote.transform.localScale.y, newNote.transform.localScale.z);
     //
     //    newNote.AddComponent<NoteController>().speed = 1;
	    // newNote.tag = "Note";
        GameObject thisNote = (GameObject) Instantiate(notePrefab, coords, Quaternion.identity);
        thisNote.transform.localScale = new Vector3(noteLength*3, thisNote.transform.localScale.y, thisNote.transform.localScale.z);
        thisNote.AddComponent<NoteController>().speed = speed;
        thisNote.tag = "Note";
    }

    private float GetTimeFloat(TimeSpan time)
    {
        float newTime = time.Seconds + (float)time.Milliseconds/1000;
        return newTime;
    }

    public void SetNote(TimeSpan metricTime){
        createNewNote = true;
        noteLength = GetTimeFloat(metricTime);
    }
}