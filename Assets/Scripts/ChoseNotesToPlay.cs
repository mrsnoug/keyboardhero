﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class ChoseNotesToPlay : MonoBehaviour
{
    public String note1 = null, note2 = null;
    private  List<String> notes = new List<String>();
    // Start is called before the first frame update
    void Awake()
    {
        if(!String.IsNullOrEmpty(note1) || !String.IsNullOrWhiteSpace(note1))
        {
            notes.Add(note1);
        }

        if(!String.IsNullOrEmpty(note2) || !String.IsNullOrWhiteSpace(note2))
        {
            notes.Add(note2);
        }
    }

    public List<String> GetNotesList()
    {
        return notes;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
